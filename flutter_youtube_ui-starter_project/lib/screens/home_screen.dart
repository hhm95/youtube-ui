import 'package:flutter/material.dart';
import 'package:flutter_youtube_ui/widgets/custom_appar.dart';
import 'package:flutter_youtube_ui/widgets/video_card.dart';

import '../data.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          CustomSliverAppBar(),
          SliverPadding(
            padding: const EdgeInsets.only(bottom: 60),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                final video = videos[index];
                return VideoCard(video: video);
              }, childCount: videos.length),
            ),
          ),
        ],
      ),
    );
  }
}
